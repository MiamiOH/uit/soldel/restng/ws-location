<?php
/**
 * Author: liaom
 * Date: 7/23/18
 * Time: 4:07 PM
 */

namespace MiamiOH\LocationWebService\Tests\Feature;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Legacy\DB\DBH;
use PHPUnit\Framework\MockObject\MockObject;

class TestCase extends \MiamiOH\RESTng\Testing\TestCase
{
    /**
     * @var MockObject
     */
    protected $dbh;

    protected function setUp()
    {
        parent::setUp();
        $databaseFactory = $this->createMock(DatabaseFactory::class);
        $this->dbh = $this->createMock(DBH::class);
        $databaseFactory->method('getHandle')->willReturn($this->dbh);

        $this->app->useService([
            'name' => 'APIDatabaseFactory',
            'object' => $databaseFactory,
            'description' => 'Mocked Database Factory'
        ]);
    }

}