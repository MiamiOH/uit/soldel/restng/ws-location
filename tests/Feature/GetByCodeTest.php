<?php
/**
 * Author: liaom
 * Date: 7/24/18
 * Time: 11:46 AM
 */

namespace MiamiOH\LocationWebService\Tests\Feature;


use MiamiOH\RESTng\App;

class GetByCodeTest extends TestCase
{
    public function testGetByCode() {
        
        $this->dbh->method('queryall_array')
            ->willReturn([
                [
                    'stvbldg_code' => 'SHD',
                    'stvbldg_desc' => 'Shideler Hall',
                    'slbbldg_street_line1' => '250 SOUTH PATTERSON AVENUE',
                    'slbbldg_city' => 'OXFORD',
                    'building_zip' => '45056',
                    'slbbldg_stat_code' => 'OH',
                    'slbbldg_camp_code' => 'O',
                    'slbbldg_street_line2' => null,
                    'slbbldg_street_line3' => null,
                    'slbbldg_street_line4' => null,
                    "latitude" => "39.50770154",
                    "longitude" => "-84.73135672",
                    "icon_url" => null,
                    "image_url" => "http://community.miamioh.edu/location/images/shd.jpg",
                    "parent_resource" => null,
                    "phone_number" => null,
                    "website" => null,
                    "wifi" => "Y",
                    "function_code" => "ACA"
                ]
            ]);

        $response = $this->getJson('/location/v3/building/SHD');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                "buildingCode" => "SHD",
                "buildingName" => "Shideler Hall",
                "buildingAddress" => "250 SOUTH PATTERSON AVENUE",
                "buildingCity" => "OXFORD",
                "buildingZip" => "45056",
                "buildingState" => "OH",
                "campusCode" => "O",
                "latitude" => "39.50770154",
                "longitude" => "-84.73135672",
                "iconURL" => null,
                "imageURL" => "http://community.miamioh.edu/location/images/shd.jpg",
                "parentResource" => null,
                "phoneNumber" => null,
                "website" => null,
                "wifi" => "Y",
                "functionCode" => "ACA"
            ]
        ]);

        $response = $this->getJson('/location/v2/building/SHD');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                "buildingCode" => "SHD",
                "buildingName" => "Shideler Hall",
                "buildingAddress" => "250 SOUTH PATTERSON AVENUE",
                "buildingCity" => "OXFORD",
                "buildingZip" => "45056",
                "buildingState" => "OH",
                "campusCode" => "O",
            ]
        ]);


    }
}