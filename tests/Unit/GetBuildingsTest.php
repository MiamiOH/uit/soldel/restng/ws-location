<?php

namespace MiamiOH\LocationWebService\Tests\Unit;

use MiamiOH\LocationWebService\Services\BuildingService;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Testing\TestCase;
use MiamiOH\RESTng\Util\Request;

class GetBuildingsTest extends TestCase
{
    private $request;
    private $dbh;
    private $buildingService;


    // set up method automatically called by PHPUnit before every test method:


    public function testNoOptions()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockNoOptions')));


        // tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryAllBuildings')));

        $response = $this->buildingService->getBuildings();
        $response = $this->buildingService->getBuildings();
        $this->assertEquals($this->mockExpectedAllBuildingsResponse(),
            $response->getPayload());

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    // test getBuildings() without any options
    public function mockExpectedAllBuildingsResponse()
    {
        $expectedReturn =
            array(
                array(
                    'buildingCode' => 'RSC',
                    'buildingName' => 'Rec Sports Center',
                    'buildingAddress' => '750 SOUTH OAK STREET',
                    'buildingCity' => 'OXFORD',
                    'buildingZip' => '45056',
                    'buildingState' => 'OH',
                    'campusCode' => 'O',
                    "latitude" => "39.50265794",
                    "longitude" => "-84.73793412",
                    "iconURL" => '',
                    "imageURL" => "http://community.miamioh.edu/location/images/rsc.jpg",
                    "parentResource" => '',
                    "phoneNumber" => '',
                    "website" => '',
                    "wifi" => "Y",
                    "functionCode" => "ATH"
                ),
                array(
                    'buildingCode' => 'DOR',
                    'buildingName' => 'Dorsey Hall',
                    'buildingAddress' => 'Dorsey Hall, Miami University, 900 East High Street',
                    'buildingCity' => 'Oxford',
                    'buildingZip' => '45056',
                    'buildingState' => 'OH',
                    'campusCode' => 'O',
                    "latitude" => "39.51067991",
                    "longitude" => "-84.72844836",
                    "iconURL" => '',
                    "imageURL" => "http://community.miamioh.edu/location/images/dor.jpg",
                    "parentResource" => '',
                    "phoneNumber" => '',
                    "website" => '',
                    "wifi" => "Y",
                    "functionCode" => "RES"
                ),
                array(
                    'buildingCode' => 'BRN',
                    'buildingName' => 'Brandon Hall',
                    'buildingAddress' => 'Brandon Hall, Miami University, 385 Tallawanda Avenue',
                    'buildingCity' => 'Oxford',
                    'buildingZip' => '45056',
                    'buildingState' => 'OH',
                    'campusCode' => 'O',
                    "latitude" => "39.51472382",
                    "longitude" => "-84.73393265",
                    "iconURL" => '',
                    "imageURL" => "http://community.miamioh.edu/location/images/brn.jpg",
                    "parentResource" => '',
                    "phoneNumber" => '',
                    "website" => '',
                    "wifi" => "Y",
                    "functionCode" => "RES"
                ),
            );

        return $expectedReturn;
    }

    public function mockQueryAllBuildings()
    {
        return array(
            array(
                'stvbldg_code' => 'RSC',
                'stvbldg_desc' => 'Rec Sports Center',
                'slbbldg_street_line1' => '750 SOUTH OAK STREET',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'OXFORD',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.50265794",
                "longitude" => "-84.73793412",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/rsc.jpg",
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ATH"
            ),
            array(
                'stvbldg_code' => 'DOR',
                'stvbldg_desc' => 'Dorsey Hall',
                'slbbldg_street_line1' => 'Dorsey Hall',
                'slbbldg_street_line2' => 'Miami University',
                'slbbldg_street_line3' => '900 East High Street',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'Oxford',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.51067991",
                "longitude" => "-84.72844836",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/dor.jpg",
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "RES"
            ),
            array(
                'stvbldg_code' => 'BRN',
                'stvbldg_desc' => 'Brandon Hall',
                'slbbldg_street_line1' => 'Brandon Hall',
                'slbbldg_street_line2' => 'Miami University',
                'slbbldg_street_line3' => '385 Tallawanda Avenue',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'Oxford',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.51472382",
                "longitude" => "-84.73393265",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/brn.jpg",
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "RES"
            ),
        );
    }

    public function mockNoOptions()
    {
        $optionsArray = array();

        return $optionsArray;
    }

    public function testBuildingOption()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockBuildingOptions')));

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryBuildingsCodes')));

        $response = $this->buildingService->getBuildings();
        $this->assertEquals($this->mockExpectedCodeResponse(),
            $response->getPayload());
    }

    // test getBuildings() with three inputted building codes

    public function mockExpectedCodeResponse()
    {
        $expectedReturn =
            array(
                array(
                    'buildingCode' => 'PSN',
                    'buildingName' => 'Pearson Hall',
                    'buildingAddress' => '700 EAST HIGH STREET',
                    'buildingCity' => 'OXFORD',
                    'buildingZip' => '45056',
                    'buildingState' => 'OH',
                    'campusCode' => 'O',
                    "latitude" => "39.51100519",
                    "longitude" => "-84.7313811",
                    "iconURL" => '',
                    "imageURL" => "http://community.miamioh.edu/location/images/psn.jpg",
                    "parentResource" => '',
                    "phoneNumber" => '',
                    "website" => '',
                    "wifi" => "Y",
                    "functionCode" => "ACA"
                ),
                array(
                    'buildingCode' => 'ALU',
                    'buildingName' => 'Alumni Hall',
                    'buildingAddress' => '350 EAST SPRING STREET',
                    'buildingCity' => 'OXFORD',
                    'buildingZip' => '45056',
                    'buildingState' => 'OH',
                    'campusCode' => 'O',
                    "latitude" => "39.50789683",
                    "longitude" => "-84.7364642",
                    "iconURL" => '',
                    "imageURL" => "http://community.miamioh.edu/location/images/alu.jpg",
                    "parentResource" => '',
                    "phoneNumber" => '',
                    "website" => '',
                    "wifi" => "Y",
                    "functionCode" => "ACA"
                ),
                array(
                    'buildingCode' => 'BEN',
                    'buildingName' => 'Benton Hall',
                    'buildingAddress' => '510 EAST HIGH STREET',
                    'buildingCity' => 'OXFORD',
                    'buildingZip' => '45056',
                    'buildingState' => 'OH',
                    'campusCode' => 'O',
                    "latitude" => "39.51074101",
                    "longitude" => "-84.7335931",
                    "iconURL" => '',
                    "imageURL" => "http://community.miamioh.edu/location/images/ben.jpg",
                    "parentResource" => '',
                    "phoneNumber" => '',
                    "website" => '',
                    "wifi" => "Y",
                    "functionCode" => "ACA"
                ),
            );

        return $expectedReturn;
    }

    public function mockQueryBuildingsCodes()
    {
        return array(
            array(
                'stvbldg_code' => 'PSN',
                'stvbldg_desc' => 'Pearson Hall',
                'slbbldg_street_line1' => '700 EAST HIGH STREET',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'OXFORD',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.51100519",
                "longitude" => "-84.7313811",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/psn.jpg",
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ACA"
            ),
            array(
                'stvbldg_code' => 'ALU',
                'stvbldg_desc' => 'Alumni Hall',
                'slbbldg_street_line1' => '350 EAST SPRING STREET',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'OXFORD',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.50789683",
                "longitude" => "-84.7364642",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/alu.jpg",
                "parentResource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ACA"
            ),
            array(
                'stvbldg_code' => 'BEN',
                'stvbldg_desc' => 'Benton Hall',
                'slbbldg_street_line1' => '510 EAST HIGH STREET',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'OXFORD',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.51074101",
                "longitude" => "-84.7335931",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/ben.jpg",
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ACA"
            ),
        );
    }

    public function mockBuildingOptions()
    {
        $optionsArray = array('BEN', 'ALU', 'PSN');

        return $optionsArray;
    }

    public function testFunctionOption()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockFunctionOptions')));

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryFunctionCodes')));

        $response = $this->buildingService->getBuildings();
        $this->assertEquals($this->mockExpectedFunctionResponse(),
            $response->getPayload());
    }

    // test getBuildings() with an inputted function code

    public function mockExpectedFunctionResponse()
    {
        $expectedReturn =
            array(
                array(
                    'buildingCode' => 'DOR',
                    'buildingName' => 'Dorsey Hall',
                    'buildingAddress' => 'Dorsey Hall, Miami University, 900 East High Street',
                    'buildingCity' => 'Oxford',
                    'buildingZip' => '45056',
                    'buildingState' => 'OH',
                    'campusCode' => 'O',
                    "latitude" => "39.51067991",
                    "longitude" => "-84.72844836",
                    "iconURL" => '',
                    "imageURL" => "http://community.miamioh.edu/location/images/dor.jpg",
                    "parentResource" => '',
                    "phoneNumber" => '',
                    "website" => '',
                    "wifi" => "Y",
                    "functionCode" => "RES"
                ),
                array(
                    'buildingCode' => 'DOD',
                    'buildingName' => 'Dodds Hall',
                    'buildingAddress' => 'Dodds Hall, Miami University, 600 S Maple Avenue',
                    'buildingCity' => 'Oxford',
                    'buildingZip' => '45056',
                    'buildingState' => 'OH',
                    'campusCode' => 'O',
                    "latitude" => "39.50310752",
                    "longitude" => "-84.733734",
                    "iconURL" => '',
                    "imageURL" => "http://community.miamioh.edu/location/images/dod.jpg",
                    "parentResource" => '',
                    "phoneNumber" => '',
                    "website" => '',
                    "wifi" => "Y",
                    "functionCode" => "RES"
                ),
                array(
                    'buildingCode' => 'PSN',
                    'buildingName' => 'Pearson Hall',
                    'buildingAddress' => '700 EAST HIGH STREET',
                    'buildingCity' => 'OXFORD',
                    'buildingZip' => '45056',
                    'buildingState' => 'OH',
                    'campusCode' => 'O',
                    "latitude" => "39.51100519",
                    "longitude" => "-84.7313811",
                    "iconURL" => '',
                    "imageURL" => "http://community.miamioh.edu/location/images/psn.jpg",
                    "parentResource" => '',
                    "phoneNumber" => '',
                    "website" => '',
                    "wifi" => "Y",
                    "functionCode" => "ACA"
                ),
                array(
                    'buildingCode' => 'HAR',
                    'buildingName' => 'Harris Dinning Hall',
                    'buildingAddress' => '500 HARRIS DRIVE',
                    'buildingCity' => 'OXFORD',
                    'buildingZip' => '45056',
                    'buildingState' => 'OH',
                    'campusCode' => 'O',
                    "latitude" => "39.50199183",
                    "longitude" => "-84.73468239",
                    "iconURL" => '',
                    "imageURL" => "http://community.miamioh.edu/location/images/har.jpg",
                    "parentResource" => '',
                    "phoneNumber" => '',
                    "website" => '',
                    "wifi" => "Y",
                    "functionCode" => "DIN"
                ),
            );

        return $expectedReturn;
    }

    public function mockQueryFunctionCodes()
    {
        return array(
            array(
                'stvbldg_code' => 'DOR',
                'stvbldg_desc' => 'Dorsey Hall',
                'slbbldg_street_line1' => 'Dorsey Hall',
                'slbbldg_street_line2' => 'Miami University',
                'slbbldg_street_line3' => '900 East High Street',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'Oxford',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.51067991",
                "longitude" => "-84.72844836",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/dor.jpg",
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "RES"
            ),
            array(
                'stvbldg_code' => 'DOD',
                'stvbldg_desc' => 'Dodds Hall',
                'slbbldg_street_line1' => 'Dodds Hall',
                'slbbldg_street_line2' => 'Miami University',
                'slbbldg_street_line3' => '600 S Maple Avenue',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'Oxford',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.50310752",
                "longitude" => "-84.733734",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/dod.jpg",
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "RES"
            ),
            array(
                'stvbldg_code' => 'PSN',
                'stvbldg_desc' => 'Pearson Hall',
                'slbbldg_street_line1' => '700 EAST HIGH STREET',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'OXFORD',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.51100519",
                "longitude" => "-84.7313811",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/psn.jpg",
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ACA"
            ),
            array(
                'stvbldg_code' => 'HAR',
                'stvbldg_desc' => 'Harris Dinning Hall',
                'slbbldg_street_line1' => '500 HARRIS DRIVE',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'OXFORD',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.50199183",
                "longitude" => "-84.73468239",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/har.jpg",
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "DIN"
            ),
        );
    }

    public function mockFunctionOptions()
    {
        $optionsArray = array('ACA', 'RES', 'DIN');

        return $optionsArray;
    }

    public function testCampusOption()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockCampusOptions')));

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryCampusCodes')));

        $response = $this->buildingService->getBuildings();
        $this->assertEquals($this->mockExpectedCampusResponse(),
            $response->getPayload());
    }

    // test getBuildings() with an inputted campus code

    public function mockExpectedCampusResponse()
    {
        $expectedReturn =
            array(
                array(
                    'buildingCode' => 'CON',
                    'buildingName' => 'Conservatory',
                    'buildingAddress' => '',
                    'buildingCity' => '',
                    'buildingZip' => '45011',
                    'buildingState' => '',
                    'campusCode' => 'H',
                    "latitude" => "39.377477",
                    "longitude" => "-84.563865",
                    "iconURL" => '',
                    "imageURL" => '',
                    "parentResource" => '',
                    "phoneNumber" => '',
                    "website" => '',
                    "wifi" => "Y",
                    "functionCode" => "ACA"
                ),
                array(
                    'buildingCode' => 'UHH',
                    'buildingName' => 'University Hall',
                    'buildingAddress' => '',
                    'buildingCity' => '',
                    'buildingZip' => '45011',
                    'buildingState' => '',
                    'campusCode' => 'H',
                    "latitude" => "39.383757",
                    "longitude" => "-84.561934",
                    "iconURL" => "",
                    "imageURL" => "",
                    "parentResource" => "",
                    "phoneNumber" => "",
                    "website" => "",
                    "wifi" => "Y",
                    "functionCode" => "ACA"
                ),
                array(
                    'buildingCode' => 'GYM',
                    'buildingName' => 'Gymnasium',
                    'buildingAddress' => '',
                    'buildingCity' => '',
                    'buildingZip' => '45011',
                    'buildingState' => '',
                    'campusCode' => 'H',
                    "latitude" => "39.376731",
                    "longitude" => "-84.563838",
                    "iconURL" => "",
                    "imageURL" => "",
                    "parentResource" => "",
                    "phoneNumber" => "",
                    "website" => "",
                    "wifi" => "Y",
                    "functionCode" => "ATH"
                ),
            );

        return $expectedReturn;
    }

    public function mockQueryCampusCodes()
    {
        return array(
            array(
                'stvbldg_code' => 'CON',
                'stvbldg_desc' => 'Conservatory',
                'slbbldg_street_line1' => '',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => '',
                'slbbldg_stat_code' => '',
                'building_zip' => '45011',
                'slbbldg_camp_code' => 'H',
                "latitude" => "39.377477",
                "longitude" => "-84.563865",
                "icon_url" => null,
                "image_url" => null,
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ACA"
            ),
            array(
                'stvbldg_code' => 'UHH',
                'stvbldg_desc' => 'University Hall',
                'slbbldg_street_line1' => '',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => '',
                'slbbldg_stat_code' => '',
                'building_zip' => '45011',
                'slbbldg_camp_code' => 'H',
                "latitude" => "39.383757",
                "longitude" => "-84.561934",
                "icon_url" => null,
                "image_url" => null,
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ACA"
            ),
            array(
                'stvbldg_code' => 'GYM',
                'stvbldg_desc' => 'Gymnasium',
                'slbbldg_street_line1' => '',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => '',
                'slbbldg_stat_code' => '',
                'building_zip' => '45011',
                'slbbldg_camp_code' => 'H',
                "latitude" => "39.376731",
                "longitude" => "-84.563838",
                "icon_url" => null,
                "image_url" => null,
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ATH"
            ),
        );
    }

    public function mockCampusOptions()
    {
        $optionsArray = array('M');

        return $optionsArray;
    }

    public function testAllOptions()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockAllOptions')));

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryAll')));

        $response = $this->buildingService->getBuildings();
        $this->assertEquals($this->mockExpectedAllResponse(),
            $response->getPayload());
    }

    // test getBuildings() with three inputted building codes, a function code and campus code

    public function mockExpectedAllResponse()
    {
        $expectedReturn =
            array(
                array(
                    'buildingCode' => 'GRD',
                    'buildingName' => 'Gardner-Harvey Library',
                    'buildingAddress' => '4200 North University Boulevard Building',
                    'buildingCity' => 'Middletown',
                    'buildingZip' => '45042',
                    'buildingState' => '',
                    'campusCode' => 'M',
                    "latitude" => "39.524692",
                    "longitude" => "-84.35704",
                    "iconURL" => "",
                    "imageURL" => "",
                    "parentResource" => "",
                    "phoneNumber" => "",
                    "website" => "",
                    "wifi" => "Y",
                    "functionCode" => "ACA"
                ),
                array(
                    'buildingCode' => 'LVH',
                    'buildingName' => 'Levey Hall',
                    'buildingAddress' => '4200 North University Boulevard Building',
                    'buildingCity' => 'Middletown',
                    'buildingZip' => '45042',
                    'buildingState' => '',
                    'campusCode' => 'M',
                    "latitude" => "39.524361",
                    "longitude" => "-84.358145",
                    "iconURL" => "",
                    "imageURL" => "",
                    "parentResource" => "",
                    "phoneNumber" => "",
                    "website" => "",
                    "wifi" => "Y",
                    "functionCode" => "ACA"
                ),
                array(
                    'buildingCode' => 'THH',
                    'buildingName' => 'Thesken Hall',
                    'buildingAddress' => '4200 North University Boulevard Building',
                    'buildingCity' => '',
                    'buildingZip' => '45042',
                    'buildingState' => '',
                    'campusCode' => 'M',
                    "latitude" => "39.524361",
                    "longitude" => "-84.359025",
                    "iconURL" => "",
                    "imageURL" => "",
                    "parentResource" => "",
                    "phoneNumber" => "",
                    "website" => "",
                    "wifi" => "Y",
                    "functionCode" => "ACA"
                ),
            );

        return $expectedReturn;
    }

    public function mockQueryAll()
    {
        return array(
            array(
                'stvbldg_code' => 'GRD',
                'stvbldg_desc' => 'Gardner-Harvey Library',
                'slbbldg_street_line1' => '4200 North University Boulevard Building',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'Middletown',
                'slbbldg_stat_code' => '',
                'building_zip' => '45042',
                'slbbldg_camp_code' => 'M',
                "latitude" => "39.524692",
                "longitude" => "-84.35704",
                "icon_url" => null,
                "image_url" => null,
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ACA"
            ),
            array(
                'stvbldg_code' => 'LVH',
                'stvbldg_desc' => 'Levey Hall',
                'slbbldg_street_line1' => '4200 North University Boulevard Building',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'Middletown',
                'slbbldg_stat_code' => '',
                'building_zip' => '45042',
                'slbbldg_camp_code' => 'M',
                "latitude" => "39.524361",
                "longitude" => "-84.358145",
                "icon_url" => null,
                "image_url" => null,
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ACA"
            ),
            array(
                'stvbldg_code' => 'THH',
                'stvbldg_desc' => 'Thesken Hall',
                'slbbldg_street_line1' => '4200 North University Boulevard Building',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => '',
                'slbbldg_stat_code' => '',
                'building_zip' => '45042',
                'slbbldg_camp_code' => 'M',
                "latitude" => "39.524361",
                "longitude" => "-84.359025",
                "icon_url" => null,
                "image_url" => null,
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ACA"
            ),
        );
    }

    public function mockAllOptions()
    {
        $optionsArray = array(
            $buildingCodes = array(
                'GRD',
                'LVH',
                'THH'
            ),
            $buildingTypes = array(
                'ACA'
            ),
            $campusCode = array(
                'M'
            )
        );

        return $optionsArray;
    }

    public function testInvalidOptions()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockInvalidOptions')));

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryInvalid')));

        $response = $this->buildingService->getBuildings();
        $this->assertEquals($this->mockExpectedInvalidResponse(),
            $response->getPayload());
    }

    // test getBuildings() with invalid parameters

    public function mockExpectedInvalidResponse()
    {
        return array();
    }

    public function mockQueryInvalid()
    {
        return array();
    }

    public function mockInvalidOptions()
    {
        $optionsArray = array(
            $buildingCodes = array(
                'BNE'
            ),
            $buildingTypes = array(
                'AAC'
            ),
            $campusCode = array(
                'C'
            )
        );

        return $optionsArray;
    }

    public function testValidCode()
    {
        $this->request->method('getResourceParam')->with('buildingCode')->willReturn('BEN');

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockValidQuery')));

        $this->dbh->method('getResourceParamKey')->with('buildingCode')->willReturn('buildingName');

        $response = $this->buildingService->getBuilding();
        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals($this->mockExpectedValidResponse(),
            $response->getPayload());
    }

    public function mockExpectedValidResponse()
    {
        $expectedReturn =
            array(
                'buildingCode' => 'BEN',
                'buildingName' => 'Benton Hall',
                'buildingAddress' => '510 EAST HIGH STREET',
                'buildingCity' => 'OXFORD',
                'buildingZip' => '45056',
                'buildingState' => 'OH',
                'campusCode' => 'O',
                "latitude" => "39.51074101",
                "longitude" => "-84.7335931",
                "iconURL" => "",
                "imageURL" => "http://community.miamioh.edu/location/images/ben.jpg",
                "parentResource" => "",
                "phoneNumber" => "",
                "website" => "",
                "wifi" => "Y",
                "functionCode" => "ACA"
            );

        return $expectedReturn;
    }

    public function mockValidQuery()
    {
        return [
            [
                'stvbldg_code' => 'BEN',
                'stvbldg_desc' => 'Benton Hall',
                'slbbldg_street_line1' => '510 EAST HIGH STREET',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'OXFORD',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.51074101",
                "longitude" => "-84.7335931",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/ben.jpg",
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ACA"
            ]
        ];
    }

    public function testInvalidCode()
    {
        //code x2gh8uhd is invalid.
        $this->request->method('getResourceParam')->with('buildingCode')->willReturn('x2gh8uhd');
        $response = $this->buildingService->getBuilding();
        $this->assertEquals(400, $response->getStatus());
    }

    public function testNoData()
    {
        $this->request->method('getResourceParam')->with('buildingCode')->willReturn('BNE');

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockNoDataQuery')));
        $response = $this->buildingService->getBuilding();
        $this->assertEquals(App::API_NOTFOUND, $response->getStatus());
        $this->assertEquals($this->mockExpectedNoDataResponse(),
            $response->getPayload());
    }

    public function mockExpectedNoDataResponse()
    {
        return array();
    }

    public function mockNoDataQuery()
    {
        return array();
    }

    protected function setUp()
    {

        //set up the mock request:
        $this->request = $this->createMock(Request::class);

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(['getResourceParamKey', 'queryall_array'])
            ->getMock();

        $db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);

        $db->method('getHandle')->willReturn($this->dbh);

        //set up the service with the mocked out resources:
        $this->buildingService = new BuildingService();
        $this->buildingService->setDatabase($db);
        $this->buildingService->setRequest($this->request);

    }

    public function mockBuildingNames()
    {
        $buildingNamesArray = array('McBride Hall', 'Phillips Hall', 'Thesken Hall');

        return $buildingNamesArray;
    }

    public function mockQueryBuildingNames()
    {
        return array(
            array(
                'stvbldg_code' => 'MCB',
                'stvbldg_desc' => 'McBride Hall',
                'slbbldg_street_line1' => 'McBride Hall',
                'slbbldg_street_line2' => 'Miami University',
                'slbbldg_street_line3' => '11 N Fisher Drive',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'Oxford',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.51066377",
                "longitude" => "-84.72701972",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/mcb.jpg",
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "RES"
            ),
            array(
                'stvbldg_code' => 'PHI',
                'stvbldg_desc' => 'Phillips Hall',
                'slbbldg_street_line1' => '420 SOUTH OAK STREET',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => 'OXFORD',
                'slbbldg_stat_code' => 'OH',
                'building_zip' => '45056',
                'slbbldg_camp_code' => 'O',
                "latitude" => "39.50535493",
                "longitude" => "-84.7369802",
                "icon_url" => null,
                "image_url" => "http://community.miamioh.edu/location/images/phi.jpg",
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ACA"
            ),
            array(
                'stvbldg_code' => 'THH',
                'stvbldg_desc' => 'Thesken Hall',
                'slbbldg_street_line1' => '4200 North University Boulevard Building',
                'slbbldg_street_line2' => '',
                'slbbldg_street_line3' => '',
                'slbbldg_street_line4' => '',
                'slbbldg_city' => '',
                'slbbldg_stat_code' => '',
                'building_zip' => '45042',
                'slbbldg_camp_code' => 'M',
                "latitude" => "39.524361",
                "longitude" => "-84.359025",
                "icon_url" => null,
                "image_url" => null,
                "parent_resource" => null,
                "phone_number" => null,
                "website" => null,
                "wifi" => "Y",
                "function_code" => "ACA"
            ),
        );
    }


    public function mockExpectedBuildingNamesResponse()
    {
        $expectedReturn =
            array(
                array(
                    'buildingCode' => 'MCB',
                    'buildingName' => 'McBride Hall',
                    'buildingAddress' => 'McBride Hall, Miami University, 11 N Fisher Drive',
                    'buildingCity' => 'Oxford',
                    'buildingZip' => '45056',
                    'buildingState' => 'OH',
                    'campusCode' => 'O',
                    "latitude" => "39.51066377",
                    "longitude" => "-84.72701972",
                    "iconURL" => "",
                    "imageURL" => "http://community.miamioh.edu/location/images/mcb.jpg",
                    "parentResource" => "",
                    "phoneNumber" => "",
                    "website" => "",
                    "wifi" => "Y",
                    "functionCode" => "RES"
                ),
                array(
                    'buildingCode' => 'PHI',
                    'buildingName' => 'Phillips Hall',
                    'buildingAddress' => '420 SOUTH OAK STREET',
                    'buildingCity' => 'OXFORD',
                    'buildingZip' => '45056',
                    'buildingState' => 'OH',
                    'campusCode' => 'O',
                    "latitude" => "39.50535493",
                    "longitude" => "-84.7369802",
                    "iconURL" => "",
                    "imageURL" => "http://community.miamioh.edu/location/images/phi.jpg",
                    "parentResource" => "",
                    "phoneNumber" => "",
                    "website" => "",
                    "wifi" => "Y",
                    "functionCode" => "ACA"
                ),
                array(
                    'buildingCode' => 'THH',
                    'buildingName' => 'Thesken Hall',
                    'buildingAddress' => '4200 North University Boulevard Building',
                    'buildingCity' => '',
                    'buildingZip' => '45042',
                    'buildingState' => '',
                    'campusCode' => 'M',
                    "latitude" => "39.524361",
                    "longitude" => "-84.359025",
                    "iconURL" => "",
                    "imageURL" => "",
                    "parentResource" => "",
                    "phoneNumber" => "",
                    "website" => "",
                    "wifi" => "Y",
                    "functionCode" => "ACA"
                ),
            );

        return $expectedReturn;
    }

    public function testBuildingNames()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockBuildingNames')));

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryBuildingNames')));

        $response = $this->buildingService->getBuildings();
        $this->assertEquals($this->mockExpectedBuildingNamesResponse(),
            $response->getPayload());
    }
}