<?php

namespace MiamiOH\LocationWebService\Resources;

use MiamiOH\RESTng\App;

class BuildingResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{
    private $serviceName = 'Location.Building';
    private $tag = "location";
    private $resourceRoot = "location";
    private $patternRoot = "/location";
    private $classPath = 'MiamiOH\LocationWebService\Services\BuildingService';

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => $this->tag,
            'description' => 'Banner location buildings Web Services'
        ));

        $this->addDefinition([
            'name' => $this->resourceRoot. '.v2.building' . '.Get.Return.Building',
            'type' => 'object',
            'properties' => [
                'buildingCode' => [
                    'type' => 'string',
                    'description' => '3 letter code of building.'
                ],
                'buildingName' => [
                    'type' => 'string',
                    'description' => 'Name of the building.'
                ],
                'buildingAddress' => [
                    'type' => 'string',
                    'description' => 'Address of the building'
                ],
                'buildingCity' => [
                    'type' => 'string',
                    'description' => 'City of the building'
                ],
                'buildingZip' => [
                    'type' => 'string',
                    'description' => 'Zip code of building'
                ],
                'buildingState' => [
                    'type' => 'string',
                    'description' => 'State building is located in'
                ],
                'campusCode' => [
                    'type' => 'string',
                    'description' => 'Campus the building is located on'
                ]
            ]
        ]);

        $this->addDefinition(array(
            'name' => $this->resourceRoot. '.v2.building' . '.Get.Return.Buildings',
            'type' => 'object',
            'properties' => array(
                'buildings' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/' . $this->resourceRoot. '.v2.building' . '.Get.Return.Building'
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->resourceRoot. '.v2.building' . '.Get.Return.Buildings.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->resourceRoot. '.v2.building' . '.Get.Return.Buildings',
            )
        ));

        $this->addDefinition(array(
            'name' => $this->resourceRoot. '.v2.building' . '.Get.Return.Building.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->resourceRoot. '.v2.building' . '.Get.Return.Building',
            )
        ));

        $this->addDefinition([
            'name' => $this->resourceRoot. '.v3.building' . '.Get.Return.Building',
            'type' => 'object',
            'properties' => [
                'buildingCode' => [
                    'type' => 'string',
                    'description' => '3 letter code of building.'
                ],
                'buildingName' => [
                    'type' => 'string',
                    'description' => 'Name of the building.'
                ],
                'buildingAddress' => [
                    'type' => 'string',
                    'description' => 'Address of the building'
                ],
                'buildingCity' => [
                    'type' => 'string',
                    'description' => 'City of the building'
                ],
                'buildingZip' => [
                    'type' => 'string',
                    'description' => 'Zip code of building'
                ],
                'buildingState' => [
                    'type' => 'string',
                    'description' => 'State building is located in'
                ],
                'campusCode' => [
                    'type' => 'string',
                    'description' => 'Campus the building is located on'
                ],
                'latitude' => array(
                    'type' => 'string',
                    'description' => 'Latitude of building'
                ),
                'longitude' => array(
                    'type' => 'string',
                    'description' => 'Longitude of building'
                ),
                'iconURL' => array(
                    'type' => 'string',
                    'description' => 'Building Icon'
                ),
                'imageURL' => array(
                    'type' => 'string',
                    'description' => 'Building Image'
                ),
                'parentResource' => array(
                    'type' => 'string',
                    'description' => 'Parent resource for building'
                ),
                'phoneNumber' => array(
                    'type' => 'string',
                    'description' => 'Phone number of building'
                ),
                'website' => array(
                    'type' => 'string',
                    'description' => 'Website of building'
                ),
                'wifi' => array(
                    'type' => 'string',
                    'description' => 'Indicates if building has wifi'
                ),
                'functionCode' => array(
                    'type' => 'string',
                    'description' => 'Function of the building'
                ),
            ]
        ]);

        $this->addDefinition(array(
            'name' => $this->resourceRoot. '.v3.building' . '.Get.Return.Buildings',
            'type' => 'object',
            'properties' => array(
                'buildings' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/' . $this->resourceRoot. '.v3.building' . '.Get.Return.Building'
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->resourceRoot. '.v3.building' . '.Get.Return.Buildings.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->resourceRoot. '.v3.building' . '.Get.Return.Buildings',
            )
        ));

        $this->addDefinition(array(
            'name' => $this->resourceRoot. '.v3.building' . '.Get.Return.Building.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->resourceRoot. '.v3.building' . '.Get.Return.Building',
            )
        ));
        $this->addDefinition(array(
            'name' => 'location.Exception',
            'type' => 'object',
            'properties' => array(
                'message' => array(
                    'type' => 'string',
                ),
                'line' => array(
                    'type' => 'integer',
                ),
                'file' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'location.Exception.404',
            'type' => 'object',
            'properties' => array()
        ));
    }

    public function registerServices(): void
    {

        $this->addService(array(
            'name' => $this->serviceName,
            'class' => $this->classPath,
            'description' => 'This service provides resources about Buildings.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                )
            ),
        ));

    }

    public function registerResources(): void
    {

        $this->addResource(array(
                'action' => 'read', //GET
                'name' => $this->resourceRoot. '.v2.building' . '.get.Building',
                'description' => 'Get a building with user specified building code',
                'summary' => 'Get a building',
                'pattern' => $this->patternRoot. '/v2/building' . '/:buildingCode',
                'service' => $this->serviceName,
                'method' => 'getBuildingV2',
                'tags' => [$this->tag],
                'returnType' => 'model',
                'params' => array(
                    'buildingCode' => array('description' => '3-6 character alphanumeric building code'),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Information of a building',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/' . $this->resourceRoot. '.v2.building' . '.Get.Return.Building.Collection',
                        )
                    ),
                    App::API_BADREQUEST =>[
                        'description' => 'Server could not understand the request due to invalid syntax',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/location.Exception',
                        ],
                    ],
                    App::API_NOTFOUND =>[
                        'description' => 'Server could not find building',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/location.Exception.404',
                        ],
                    ],
                    App::API_FAILED =>[
                        'description' => 'Server Error',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/location.Exception',
                        ],
                    ],
                )
            )
        );

        $this->addResource(array(
                'action' => 'read', //GET
                'name' => $this->resourceRoot. '.v2.building' . '.get.Buildings',
                'description' => "##### Description:\nThis resource provides get buildings function to the consumer application. This return a collection of building info based on filters.",
                'summary' => 'Get a collection of buildings',
                'pattern' => $this->patternRoot. '/v2/building',
                'service' => $this->serviceName,
                'tags' => [$this->tag],
                'method' => 'getBuildingsV2',
                'returnType' => 'collection',
                'options' => array(
                    'buildingCodes' => array(
                        'type' => 'list',
                        'description' => 'Codes for one or 
        	more buildings. e.g. GYM,PHE,RSC.'
                    ),
//                    'buildingTypes' => array(
//                        'type' => 'list',
//                        'description' => 'Types of
//    		buildings to query for. Possible values are aca, adm, ath, cul, din, res.'
//                    ),
                    'campusCodes' => array(
                        'type' => 'list',
                        'description' => 'One letter code for 
    		campus. e.g. O,H,M.'
                    ),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Collection of buildings',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/' . $this->resourceRoot. '.v2.building' . '.Get.Return.Buildings.Collection',
                        )
                    ),
                    App::API_BADREQUEST =>[
                        'description' => 'Server could not understand the request due to invalid syntax',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/location.Exception',
                        ],
                    ],
                    App::API_NOTFOUND =>[
                        'description' => 'Server could not find building',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/location.Exception.404',
                        ],
                    ],
                    App::API_FAILED =>[
                        'description' => 'Server Error',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/location.Exception',
                        ],
                    ],
                )
            )
        );

        $this->addResource(array(
                'action' => 'read', //GET
                'name' => $this->resourceRoot. '.v3.building' . '.get.Building',
                'description' => 'Get a building with user specified building code',
                'summary' => 'Get a building',
                'pattern' => $this->patternRoot. '/v3/building' . '/:buildingCode',
                'service' => $this->serviceName,
                'method' => 'getBuilding',
                'tags' => [$this->tag],
                'returnType' => 'model',
                'params' => array(
                    'buildingCode' => array('description' => '3-6 character alphanumeric building code'),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Information of a building',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/' . $this->resourceRoot. '.v3.building' . '.Get.Return.Building.Collection',
                        )
                    ),
                    App::API_BADREQUEST =>[
                        'description' => 'Server could not understand the request due to invalid syntax',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/location.Exception',
                        ],
                    ],
                    App::API_NOTFOUND =>[
                        'description' => 'Server could not find building',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/location.Exception.404',
                        ],
                    ],
                    App::API_FAILED =>[
                        'description' => 'Server Error',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/location.Exception',
                        ],
                    ],
                )
            )
        );

        $this->addResource(array(
                'action' => 'read', //GET
                'name' => $this->resourceRoot. '.v3.building' . '.get.Buildings',
                'description' => "##### Description:\nThis resource provides get buildings function to the consumer application. This return a collection of building info based on filters.",
                'summary' => 'Get a collection of buildings',
                'pattern' => $this->patternRoot. '/v3/building',
                'service' => $this->serviceName,
                'tags' => [$this->tag],
                'method' => 'getBuildings',
                'returnType' => 'collection',
                'options' => array(
                    'buildingCodes' => array(
                        'type' => 'list',
                        'description' => 'Codes for one or 
        	more buildings. e.g. GYM,PHE,RSC.'
                    ),
                    'buildingTypes' => array(
                        'type' => 'list',
                        'description' => 'Types of 
    		buildings to query for. Possible values are aca, adm, ath, cul, din, res.'
                    ),
                    'campusCodes' => array(
                        'type' => 'list',
                        'description' => 'One letter code for 
    		campus. e.g. O,H,M.'
                    ),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Collection of buildings',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/' . $this->resourceRoot. '.v3.building' . '.Get.Return.Buildings.Collection',
                        )
                    ),
                    App::API_BADREQUEST =>[
                        'description' => 'Server could not understand the request due to invalid syntax',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/location.Exception',
                        ],
                    ],
                    App::API_NOTFOUND =>[
                        'description' => 'Server could not find building',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/location.Exception.404',
                        ],
                    ],
                    App::API_FAILED =>[
                        'description' => 'Server Error',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/location.Exception',
                        ],
                    ],
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}