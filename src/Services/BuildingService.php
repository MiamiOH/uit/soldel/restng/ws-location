<?php

namespace MiamiOH\LocationWebService\Services;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service;

class BuildingService extends Service
{

    private $database = '';
    private $datasource_name = 'MUWS_GEN_PROD'; // secure datasource


    /************************************************/
    /**********Setter Dependency Injection***********/
    /***********************************************/

    // Inject the database object provided by the framework
    public function setDatabase($database)
    {
        $this->database = $database->getHandle($this->datasource_name);
    }

    // GET(read/view) the Ballot Information
    public function getBuilding()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $offset = $request->getOffset();
        $limit = $request->getLimit();

        $buildingCode = strtoupper($request->getResourceParam('buildingCode'));
        if (preg_match("/^[A-Z0-9]\w{2,5}$/", $buildingCode) !== 1) {
            $response->setStatus(App::API_BADREQUEST);

            return $response;
        }

        // Run query and build arrays for classroom info
        $results = $this->database->queryall_array($this->buildQueryBuilding(true,
            ""), $buildingCode);
        $payload = array();
        foreach ($results as $row) {
            $payload = $this->buildRecord($row);
        }

        // If payload has no length return not found
        if (sizeof($payload) <= 0) {
            $response->setStatus(App::API_NOTFOUND);

            return $response;
        }

        // Response was successful and Return information
        $response->setStatus(App::API_OK);
        $response->setPayload($payload);

        return $response;

    }

    public function getBuildingV2()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $offset = $request->getOffset();
        $limit = $request->getLimit();

        $buildingCode = strtoupper($request->getResourceParam('buildingCode'));
        if (preg_match("/^[A-Z0-9]\w{2,5}$/", $buildingCode) !== 1) {
            $response->setStatus(App::API_BADREQUEST);

            return $response;
        }

        // Run query and build arrays for classroom info
        $results = $this->database->queryall_array($this->buildQueryBuilding(true,
            ""), $buildingCode);
        $payload = array();
        foreach ($results as $row) {
            $payload = $this->buildRecordV2($row);
        }

        // If payload has no length return not found
        if (sizeof($payload) <= 0) {
            $response->setStatus(App::API_NOTFOUND);

            return $response;
        }

        // Response was successful and Return information
        $response->setStatus(App::API_OK);
        $response->setPayload($payload);

        return $response;

    }

    private function buildQueryBuilding($isSingle, $whereString)
    {
        $queryString = "select distinct stvbldg_code, stvbldg_desc, slbbldg_street_line1, 
		slbbldg_street_line2, slbbldg_street_line3, slbbldg_street_line4, slbbldg_city, 
		slbbldg_stat_code, building_zip, slbbldg_camp_code, latitude, longitude, icon_url, 
		image_url, parent_resource, phone_number, website, wifi, function_code
		from stvbldg, slbbldg, MUMISCMGR.MUL_BUILDING_FUNCTION, MUMISCMGR.MUL_BUILDING 
		where stvbldg_code = slbbldg_bldg_code 
		and stvbldg_code = MUMISCMGR.MUL_BUILDING.building_code 
		and stvbldg_code = MUMISCMGR.MUL_BUILDING_FUNCTION.building_code " . $whereString;
        // if only querying for single building append the additional where clause to
        // the parameter for building_code
        if ($isSingle) {
            $queryString .= " and stvbldg_code = ?";
        }

        return $queryString;
    }

    private function buildRecordV2($row)
    {

        $record = array(
            'buildingCode' => isset($row['stvbldg_code'])?$row['stvbldg_code']:'',
            'buildingName' => isset($row['stvbldg_desc'])?$row['stvbldg_desc']:'',
            'buildingAddress' => isset($row['slbbldg_street_line1'])?$row['slbbldg_street_line1']:'',
            'buildingCity' => isset($row['slbbldg_city'])?$row['slbbldg_city']:'',
            'buildingZip' => isset($row['building_zip'])?$row['building_zip']:'',
            'buildingState' => isset($row['slbbldg_stat_code'])?$row['slbbldg_stat_code']:'',
            'campusCode' => isset($row['slbbldg_camp_code'])?$row['slbbldg_camp_code']:'',
        );

        //check and see if address spans multiple columns
        if ($row['slbbldg_street_line2'] != '') {
            if ($record['buildingAddress'] != '') {
                $record['buildingAddress'] .= ", ";
            }
            $record['buildingAddress'] .= $row['slbbldg_street_line2'];
        }

        if ($row['slbbldg_street_line3'] != '') {
            if ($record['buildingAddress'] != '') {
                $record['buildingAddress'] .= ", ";
            }
            $record['buildingAddress'] .= $row['slbbldg_street_line3'];
        }

        if ($row['slbbldg_street_line4'] != '') {
            if ($record['buildingAddress'] != '') {
                $record['buildingAddress'] .= ", ";
            }
            $record['buildingAddress'] .= $row['slbbldg_street_line4'];
        }

        return $record;
    }

    private function buildRecord($row)
    {

        $record = array(
            'buildingCode' => isset($row['stvbldg_code'])?$row['stvbldg_code']:'',
            'buildingName' => isset($row['stvbldg_desc'])?$row['stvbldg_desc']:'',
            'buildingAddress' => isset($row['slbbldg_street_line1'])?$row['slbbldg_street_line1']:'',
            'buildingCity' => isset($row['slbbldg_city'])?$row['slbbldg_city']:'',
            'buildingZip' => isset($row['building_zip'])?$row['building_zip']:'',
            'buildingState' => isset($row['slbbldg_stat_code'])?$row['slbbldg_stat_code']:'',
            'campusCode' => isset($row['slbbldg_camp_code'])?$row['slbbldg_camp_code']:'',
            'latitude' => isset($row['latitude'])?$row['latitude']:'',
            'longitude' => isset($row['longitude'])?$row['longitude']:'',
            'iconURL' => isset($row['icon_url'])?$row['icon_url']:'',
            'imageURL' => isset($row['image_url'])?$row['image_url']:'',
            'parentResource' => isset($row['parent_resource'])?$row['parent_resource']:'',
            'phoneNumber' => isset($row['phone_number'])?$row['phone_number']:'',
            'website' => isset($row['website'])?$row['website']:'',
            'wifi' => isset($row['wifi'])?$row['wifi']:'',
            'functionCode' => isset($row['function_code'])?$row['function_code']:'',
        );

        //check and see if address spans multiple columns
        if ($row['slbbldg_street_line2'] != '') {
            if ($record['buildingAddress'] != '') {
                $record['buildingAddress'] .= ", ";
            }
            $record['buildingAddress'] .= $row['slbbldg_street_line2'];
        }

        if ($row['slbbldg_street_line3'] != '') {
            if ($record['buildingAddress'] != '') {
                $record['buildingAddress'] .= ", ";
            }
            $record['buildingAddress'] .= $row['slbbldg_street_line3'];
        }

        if ($row['slbbldg_street_line4'] != '') {
            if ($record['buildingAddress'] != '') {
                $record['buildingAddress'] .= ", ";
            }
            $record['buildingAddress'] .= $row['slbbldg_street_line4'];
        }

        return $record;
    }

    public function getBuildings()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $offset = $request->getOffset();
        $limit = $request->getLimit();

        $whereString = $this->buildWhereString($options);

        // Run query and build arrays for classroom info
        $results = $this->database->queryall_array($this->buildQueryBuilding(false,
            $whereString));
        $payload = array();
        foreach ($results as $row) {
            $payload[] = $this->buildRecord($row);
        }

        // If payload has no length return not found
        if (sizeof($payload) <= 0) {
            $response->setStatus(App::API_NOTFOUND);

            return $response;
        }

        // Response was successful and Return information
        $response->setStatus(App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    public function getBuildingsV2()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $offset = $request->getOffset();
        $limit = $request->getLimit();

        $whereString = $this->buildWhereString($options);

        // Run query and build arrays for classroom info
        $results = $this->database->queryall_array($this->buildQueryBuilding(false,
            $whereString));
        $payload = array();
        foreach ($results as $row) {
            $payload[] = $this->buildRecordV2($row);
        }

        // If payload has no length return not found
        if (sizeof($payload) <= 0) {
            $response->setStatus(App::API_NOTFOUND);

            return $response;
        }

        // Response was successful and Return information
        $response->setStatus(App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    private function buildWhereString($info)
    {
        $whereString = "";
        // check and see if user is searching for multiple specific buildings by code
        if (isset($info['buildingCodes'])) {
            $whereString = " and (stvbldg_code = '" . strtoupper($info['buildingCodes'][0]) . "'";
            unset($info['buildingCodes'][0]);
            foreach ($info['buildingCodes'] as $data) {
                $whereString .= " or stvbldg_code = '" . strtoupper($data) . "'";
            }
            $whereString .= ")";
        }
        // check and see if user is searching for specific building types
        if (isset($info['buildingTypes'])) {
            $whereString .= " and (function_code = '" . strtoupper($info['buildingTypes'][0]) . "'";
            unset($info['buildingTypes'][0]);
            foreach ($info['buildingTypes'] as $data) {
                $whereString .= " or function_code = '" . strtoupper($data) . "'";
            }
            $whereString .= ")";
        }
        // check and see if user is searching for specific campuses
        if (isset($info['campusCodes'])) {
            $whereString .= " and (slbbldg_camp_code = '" . strtoupper($info['campusCodes'][0]) . "'";
            unset($info['campusCodes'][0]);
            foreach ($info['campusCodes'] as $data) {
                $whereString .= " or slbbldg_camp_code = '" . strtoupper($data) . "'";
            }
            $whereString .= ")";
        }

        return $whereString;
    }

}
