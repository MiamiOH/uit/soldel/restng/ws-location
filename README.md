# Location Web Service

Location web service(s) provides information of different type of location 
(e.g. buildings, fields, etc.) in the Miami University.

## Building Web Service

Building web service provides building information including address, 
GEO data, image, function type, etc.

Building web service was originally written in RestServer. Original Building web service pulled data from two Banner tables (stvbldg and slbbldg) and two custom tables (mul_building and mul_building_function). Those two custom tables were not updated and contains old buildings information. Since no one maintains those custom tables, new Building web service will not retrieve data from those two custom tables, but from Banner tables only.

### Resources:

#### GET /location/v2/building/{buildingCode}

This resource is used to retrieve a [building](./docs/models/building.md) by building code.

#### GET /location/v2/building

This resource is used to retrieve a collection of [building](./docs/models/building.md)
by specifying different combination of [filters](./docs/filters/building.md).