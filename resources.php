<?php

return [
    'resources' => [
        'location' => [
            MiamiOH\LocationWebService\Resources\BuildingResourceProvider::class,
        ],
    ]
];