# Building Data Model

|Key|Type|Sample Value|Comments|
|:-----:|:-------:|:-------:|:-------:|
|buildingCode|string|POR| |
|buildingName|string|Porter Hall| |
|buildingAddress|string|Porter Hall, Miami University, 601 South Oak Street| |
|buildingCity|string|Oxford| |
|buildingZip|string|45056| |
|buildingState|string|OH| |
|campusCode|string|O| |