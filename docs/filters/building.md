# Building Filters

## General Expectations
 - each filter has `AND` relation with other filters.
 - each comma separated value in a filter has `OR` relation with other values.
 
## Accepted Filters
 
### buildingCodes
 
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?buildingCodes=`code`|`uri`?buildingCodes=POR|
|a list of string, comma separated|`uri`?buildingCodes=`code 1`,`code 2`,...,`code n`>|`uri`?buildingCodes=POR,MMH|

### buildingTypes
 
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?buildingTypes=`type`|`uri`?buildingTypes=RES|
|a list of string, comma separated|`uri`?buildingTypes=`type 1`,`type 2`,...,`type n`>|`uri`?buildingTypes=RES,DIN|

### campusCode
 
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?campusCode=`code`|`uri`?campusCode=O|
|a list of string, comma separated|`uri`?campusCode=`code 1`,`code 2`,...,`code n`>|`uri`?campusCode=O,H|

### name
 
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?name=`code`|`uri`?name=Harris Dining Hall|
|a list of string, comma separated|`uri`?name=`code 1`,`code 2`,...,`code n`>|`uri`?name=Harris Dining Hall,Clawson Hall|

